from newborn.app.db_session import db
from newborn.model.feeding import Feeding
from newborn.model.diaper import Diaper

db.create_all()
