#!/usr/bin/env bash

rm -f newborn.zip
zip -rq newborn.zip .
aws lambda update-function-code --function-name newborn --zip-file fileb://newborn.zip
rm -f newborn.zip
