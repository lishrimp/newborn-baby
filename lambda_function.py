from __future__ import print_function

import time
from datetime import datetime
from random import choice

from newborn.app.newborn_baby import NewbornBaby
from newborn.language.en import WELCOME_SPEECH, WELCOME_REPROMPT_SPEECH, GOODBYE_SPEECH, LOG_FEEDING_SPEECH, \
    LOG_FEEDING_REPROMPT_SPEECH, LOG_DIAPER_SPEECH, LOG_DIAPER_REPROMPT_SPEECH, LAST_FEEDING_JUST_NOW_SPEECH, \
    LAST_FEEDING_MORE_THAN_A_DAY_SPEECH, LAST_FEEDING_MORE_THAN_AN_HOUR_SPEECH, LAST_FEEDING_LESS_THAN_AN_HOUR_SPEECH, \
    LAST_FEEDING_NOT_FOUND, LAST_DIAPER_JUST_NOW_SPEECH, LAST_DIAPER_MORE_THAN_A_DAY_SPEECH, \
    LAST_DIAPER_MORE_THAN_AN_HOUR_SPEECH, LAST_DIAPER_LESS_THAN_AN_HOUR_SPEECH, LAST_DIAPER_NOT_FOUND, CARD_TITLE, \
    HELP_SPEECH

ALEXA_SKILL_ID = 'amzn1.ask.skill.c92dae13-6b05-4394-b0ae-e4f7cfc3c11f'
LIST_NAME = 'newborn baby'
LIST_API_ENDPOINT = 'https://api.amazonalexa.com'


# --------------- Helpers that build all of the responses ----------------------

def datetime_to_timestamp(dt):
    return time.mktime(dt.timetuple())


def timestamp_to_datetime(ts):
    return datetime.fromtimestamp(ts)


def build_speechlet_response(title, output, reprompt_text, should_end_session):
    return {
        'outputSpeech': {
            'type': 'PlainText',
            'text': output
        },
        'card': {
            'type': 'Simple',
            'title': title,
            'content': output
        },
        'reprompt': {
            'outputSpeech': {
                'type': 'PlainText',
                'text': reprompt_text
            }
        },
        'shouldEndSession': should_end_session
    }


def build_response(session_attributes, speechlet_response):
    return {
        'version': '1.0',
        'sessionAttributes': session_attributes,
        'response': speechlet_response
    }


# --------------- Functions that control the skill's behavior ------------------

def get_welcome_response():
    session_attributes = {}
    card_title = "Welcome"
    speech_output = choice(WELCOME_SPEECH)

    # If the user either does not reply to the welcome message or says something
    # that is not understood, they will be prompted again with this text.
    reprompt_text = choice(WELCOME_REPROMPT_SPEECH)
    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def get_help_response():
    session_attributes = {}
    card_title = "Help"
    speech_output = choice(HELP_SPEECH)

    should_end_session = False
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, None, should_end_session))


def handle_session_end_request():
    card_title = "Good bye"
    speech_output = choice(GOODBYE_SPEECH)

    # Setting this to true ends the session and exits the skill.
    should_end_session = True
    return build_response({}, build_speechlet_response(
        card_title, speech_output, None, should_end_session))


def log_baby_feeding(intent, session):
    card_title = CARD_TITLE[intent['name']]
    session_attributes = {}
    should_end_session = False

    if intent['slots']['Food'].get('value'):
        user_id = session['user']['userId']
        meal = intent['slots']['Food']['value']

        with NewbornBaby() as nb:
            nb.new_feeding_record(user_id=user_id,
                                  feeding_time=datetime.now(),
                                  meal=meal)

        speech_output = choice(LOG_FEEDING_SPEECH).format(meal)
        reprompt_text = choice(LOG_FEEDING_REPROMPT_SPEECH)

        should_end_session = True
    else:
        return build_delegate_response()

        # speech_output = "I'm not sure what your baby had. " \
        #                 "Please try again."
        # reprompt_text = "I'm not sure what your baby had. " \
        #                 "You can tell me what your baby had by saying, " \
        #                 "The baby had breast milk, or the baby had baby formula."
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def log_diaper_change(intent, session):
    card_title = CARD_TITLE[intent['name']]
    session_attributes = {}
    should_end_session = False

    if intent['slots']['Contents'].get('value'):
        user_id = session['user']['userId']
        contents = intent['slots']['Contents']['value']

        with NewbornBaby() as nb:
            nb.new_diaper_record(user_id=user_id,
                                 change_time=datetime.now(),
                                 contents=contents)

        speech_output = choice(LOG_DIAPER_SPEECH).format(contents)
        reprompt_text = choice(LOG_DIAPER_REPROMPT_SPEECH)

        should_end_session = True
    else:
        return build_delegate_response()
        # speech_output = "I'm not sure what was in the diaper. " \
        #                 "Please try again."
        # reprompt_text = "I'm not sure what was in the diaper. " \
        #                 "You can say it was dirty diaper, wet diaper, or clean diaper."
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def get_elapsed(datetime_obj):
    elapsed = datetime.now() - datetime_obj
    total_seconds = elapsed.total_seconds()
    minutes = int(total_seconds // 60)
    hours = int((total_seconds - minutes * 60) // 3600)
    return hours, minutes


def get_last_baby_feeding(intent, session):
    reprompt_text = None
    session_attributes = {}

    user_id = session['user']['userId']

    with NewbornBaby() as nb:
        last_feeding = nb.get_last_feeding_record(user_id=user_id)

    if last_feeding:
        last_feeding_time = last_feeding.feeding_time
        last_meal = last_feeding.meal
        hours, minutes = get_elapsed(last_feeding_time)

        if hours == 0 and minutes == 0:
            speech_output = choice(LAST_FEEDING_JUST_NOW_SPEECH).format(last_meal)
        elif hours > 24:
            speech_output = choice(LAST_FEEDING_MORE_THAN_A_DAY_SPEECH)
        else:
            if hours > 0:
                speech_output = choice(LAST_FEEDING_MORE_THAN_AN_HOUR_SPEECH).format(last_meal, hours, minutes)
            else:
                speech_output = choice(LAST_FEEDING_LESS_THAN_AN_HOUR_SPEECH).format(last_meal, minutes)

        should_end_session = True
    else:
        speech_output = choice(LAST_FEEDING_NOT_FOUND)
        should_end_session = False

    # Setting reprompt_text to None signifies that we do not want to reprompt
    # the user. If the user does not respond or says something that is not
    # understood, the session will end.
    card_title = CARD_TITLE[intent['name']]
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


def get_last_diaper_change(intent, session):
    reprompt_text = None
    session_attributes = {}

    user_id = session['user']['userId']

    with NewbornBaby() as nb:
        last_change = nb.get_last_diaper_record(user_id=user_id)

    if last_change:
        last_change_time = last_change.change_time
        last_contents = last_change.contents
        hours, minutes = get_elapsed(last_change_time)

        if hours == 0 and minutes == 0:
            speech_output = choice(LAST_DIAPER_JUST_NOW_SPEECH).format(last_contents)
        elif hours > 24:
            speech_output = choice(LAST_DIAPER_MORE_THAN_A_DAY_SPEECH)
        else:
            if hours > 0:
                speech_output = choice(LAST_DIAPER_MORE_THAN_AN_HOUR_SPEECH).format(last_contents, hours, minutes)
            else:
                speech_output = choice(LAST_DIAPER_LESS_THAN_AN_HOUR_SPEECH).format(last_contents, minutes)

        should_end_session = True
    else:
        speech_output = choice(LAST_DIAPER_NOT_FOUND)
        should_end_session = False

    # Setting reprompt_text to None signifies that we do not want to reprompt
    # the user. If the user does not respond or says something that is not
    # understood, the session will end.
    card_title = CARD_TITLE[intent['name']]
    return build_response(session_attributes, build_speechlet_response(
        card_title, speech_output, reprompt_text, should_end_session))


# --------------- Events ------------------

def on_session_started(session_started_request, session):
    """ Called when the session starts """

    print("on_session_started requestId=" + session_started_request['requestId']
          + ", sessionId=" + session['sessionId'])


def on_launch(launch_request, session):
    """ Called when the user launches the skill without specifying what they want
    """

    print("on_launch requestId=" + launch_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    # Dispatch to your skill's launch
    return get_welcome_response()


def build_delegate_response():
    return build_response({}, {"directives": [{"type": "Dialog.Delegate", }]})


def on_intent(intent_request, session):
    """ Called when the user specifies an intent for this skill """

    print("on_intent requestId=" + intent_request['requestId'] +
          ", sessionId=" + session['sessionId'])

    intent = intent_request['intent']
    intent_name = intent_request['intent']['name']

    # Dispatch to your skill's intent handlers
    if intent_name == "FeedBabyIntent":
        return log_baby_feeding(intent, session)
    elif intent_name == "WhenDidBabyEatIntent":
        return get_last_baby_feeding(intent, session)
    if intent_name == "ChangeDiaperIntent":
        return log_diaper_change(intent, session)
    elif intent_name == "WhenWasDiaperChangeIntent":
        return get_last_diaper_change(intent, session)
    elif intent_name == "AMAZON.HelpIntent":
        return get_help_response()
    elif intent_name == "AMAZON.CancelIntent" or intent_name == "AMAZON.StopIntent":
        return handle_session_end_request()
    else:
        raise ValueError("Invalid intent")


def on_session_ended(session_ended_request, session):
    """ Called when the user ends the session.

    Is not called when the skill returns should_end_session=true
    """
    print("on_session_ended requestId=" + session_ended_request['requestId'] +
          ", sessionId=" + session['sessionId'])
    # add cleanup logic here
    return handle_session_end_request()


# --------------- Main handler ------------------

def lambda_handler(event, context):
    """ Route the incoming request based on type (LaunchRequest, IntentRequest,
    etc.) The JSON body of the request is provided in the event parameter.
    """
    print("event.session.application.applicationId=" +
          event['session']['application']['applicationId'])

    if event['session']['application']['applicationId'] != ALEXA_SKILL_ID:
        raise ValueError("Invalid Application ID")

    if event['session']['new']:
        on_session_started({'requestId': event['request']['requestId']},
                           event['session'])

    if event['request']['type'] == "LaunchRequest":
        return on_launch(event['request'], event['session'])
    elif event['request']['type'] == "IntentRequest":
        return on_intent(event['request'], event['session'])
    elif event['request']['type'] == "SessionEndedRequest":
        return on_session_ended(event['request'], event['session'])
