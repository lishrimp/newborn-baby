from newborn.app.db_session import db, session
from newborn.controller.diaper_controller import DiaperController
from newborn.controller.feeding_controller import FeedingController


class NewbornBaby:
    def __enter__(self):
        try:
            db.create_all()
        except Exception as e:
            print(e)

        return self

    def __exit__(self, type, value, tb):
        session.close()

    def new_feeding_record(self, user_id, feeding_time, meal):
        feeding_ctl = FeedingController()
        feeding_ctl.new_feeding_record(user_id, feeding_time, meal)

    def get_feeding_records(self, user_id):
        feeding_ctl = FeedingController()
        return feeding_ctl.get_feeding_records(user_id)

    def get_last_feeding_record(self, user_id):
        feeding_ctl = FeedingController()
        return feeding_ctl.get_last_feeding_record(user_id)

    def new_diaper_record(self, user_id, change_time, contents):
        diaper_ctl = DiaperController()
        diaper_ctl.new_diaper_record(user_id, change_time, contents)

    def get_diaper_records(self, user_id):
        diaper_ctl = DiaperController()
        return diaper_ctl.get_diaper_records(user_id)

    def get_last_diaper_record(self, user_id):
        diaper_ctl = DiaperController()
        return diaper_ctl.get_last_diaper_record(user_id)
