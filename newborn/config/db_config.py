from os import environ

db_host = environ.get('RdsEndpoint')
db_username = 'user'
db_password = 'mydbinstancepasswordyes'
db_name = 'newborn'

SQLALCHEMY_DATABASE_URI = f'mysql+pymysql://{db_username}:{db_password}@{db_host}/{db_name}'
SQLALCHEMY_TRACK_MODIFICATIONS = True
