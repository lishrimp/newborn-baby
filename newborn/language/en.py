WELCOME_SPEECH = [
    "Did you feed the baby? Did you change the diaper?",
    "I'm listening!"
]

WELCOME_REPROMPT_SPEECH = [
    "You can say, record breast milk or formula. " \
    "or record a wet diaper or a dirty diaper. "
]

HELP_SPEECH = [
    "Newborn Baby helps you to keep track of baby feedings and diaper changes. " \
    "For baby feedings, try saying, open Newborn Baby and record breast feeding. " \
    "You can ask about the last feeding by saying, ask Newborn Baby about the last feeding. " \
    "For diaper changes, try saying, open Newborn Baby and record a dirty diaper" \
    "You can ask about the last diaper change by saying, ask Newborn Baby about the last diaper change. " \
]

GOODBYE_SPEECH = [
    "Sweet dream!",
    "See you soon",
    "Good bye",
    "Bye"
]

LOG_FEEDING_SPEECH = [
    "{0}, noted",
    "{0}, recorded",
    "{0}, sounds good",
    "{0}, got it!"
    # "You've just fed the baby {0}. " \
    # "You can ask me later about the last meal by saying, " \
    # "Ask Newborn Baby when was the last feeding"
]

LOG_FEEDING_REPROMPT_SPEECH = [
    "You shouldn't be here!"
]

LOG_DIAPER_SPEECH = [
    "{0} diaper recorded",
    "{0} diaper, noted",
    "{0} diaper, got it"
    # "You've just changed the baby's {0} diaper. " \
    # "You can ask me later about the last diaper change by saying, " \
    # "Ask Newborn Baby when was the last diaper change"
]

LOG_DIAPER_REPROMPT_SPEECH = [
    "You shouldn't be here!"
]

LAST_FEEDING_JUST_NOW_SPEECH = [
    "Your baby had {0} just now!"
]

LAST_FEEDING_MORE_THAN_A_DAY_SPEECH = [
    "Your baby had a meal more than a day ago. Hmm.. That doesn't sound right."
]

LAST_FEEDING_MORE_THAN_AN_HOUR_SPEECH = [
    "Your baby had {0} {1} hours and {2} minutes ago"
]

LAST_FEEDING_LESS_THAN_AN_HOUR_SPEECH = [
    "Your baby had {0} {1} minutes ago"
]

LAST_FEEDING_NOT_FOUND = [
    "I don't have any feeding record yet. " \
    "You can say, record breast milk or baby formula."
]

LAST_DIAPER_JUST_NOW_SPEECH = [
    "Your baby changed a {0} diaper just now!"
]

LAST_DIAPER_MORE_THAN_A_DAY_SPEECH = [
    "Your baby changed a diaper more than a day ago. Hmm.. That doesn't sound right."
]

LAST_DIAPER_MORE_THAN_AN_HOUR_SPEECH = [
    "Your baby changed a {0} diaper {1} hours and {2} minutes ago"
]

LAST_DIAPER_LESS_THAN_AN_HOUR_SPEECH = [
    "Your baby changed a {0} diaper {1} minutes ago"
]

LAST_DIAPER_NOT_FOUND = [
    "I don't have any diaper change record yet. " \
    "You can say, record wet diaper or dirty diaper."
]

CARD_TITLE = {
    'FeedBabyIntent': 'Baby Feeding',
    'WhenDidBabyEatIntent': 'Last Feeding',
    'ChangeDiaperIntent': 'Diaper Change',
    'WhenWasDiaperChangeIntent': 'Last Diaper Change',
    'AMAZON.HelpIntent': 'Help',
    'AMAZON.CancelIntent': 'Cancel',
    'AMAZON.StopIntent': 'Stop'
}
