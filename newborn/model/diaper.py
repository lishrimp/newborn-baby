from sqlalchemy import Column, Integer, String, DateTime

from newborn.app.db_session import db, Base


class Diaper(Base):
    __tablename__ = 'diaper'

    diaper_id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(String(256))
    change_time = Column(DateTime)
    contents = Column(String(30))
    created_time = Column(DateTime)

    @classmethod
    def from_user_id(cls, user_id):
        return db.query(Diaper).filter_by(user_id=user_id)

    def __repr__(self):
        return f'<{self.__class__.__name__} diaper_id={self.diaper_id}, ' \
               f'user_id={self.user_id}, change_time={self.change_time}, ' \
               f'contents={self.contents}, created_time={self.created_time}'
