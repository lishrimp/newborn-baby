from sqlalchemy import Column, Integer, String, DateTime

from newborn.app.db_session import db, Base


class Feeding(Base):
    __tablename__ = 'feeding'

    feeding_id = Column(Integer, primary_key=True, autoincrement=True)
    user_id = Column(String(256))
    feeding_time = Column(DateTime)
    meal = Column(String(30))
    created_time = Column(DateTime)

    @classmethod
    def from_user_id(cls, user_id):
        return db.query(Feeding).filter_by(user_id=user_id)

    def __repr__(self):
        return f'<{self.__class__.__name__} feeding_id={self.feeding_id}, ' \
               f'user_id={self.user_id}, feeding_time={self.feeding_time}, ' \
               f'meal={self.meal}, created_time={self.created_time}'
