from datetime import datetime

from sqlalchemy import desc

from newborn.app.db_session import session
from newborn.model.feeding import Feeding


class FeedingController():
    def new_feeding_record(self, user_id, feeding_time, meal):
        feeding = Feeding(user_id=user_id,
                          feeding_time=feeding_time,
                          meal=meal,
                          created_time=datetime.now())
        session.add(feeding)
        session.commit()

    def get_feeding_records(self, user_id):
        items = session.query(Feeding).filter_by(user_id=user_id).all()
        return items


    def get_last_feeding_record(self, user_id):
        last_item = session.query(Feeding).filter_by(user_id=user_id).order_by(desc(Feeding.feeding_id)).first()
        return last_item
