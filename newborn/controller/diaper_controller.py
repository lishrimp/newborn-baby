from datetime import datetime

from sqlalchemy import desc

from newborn.app.db_session import session
from newborn.model.diaper import Diaper


class DiaperController():
    def new_diaper_record(self, user_id, change_time, contents):
        feeding = Diaper(user_id=user_id,
                         change_time=change_time,
                         contents=contents,
                         created_time=datetime.now())
        session.add(feeding)
        session.commit()

    def get_diaper_records(self, user_id):
        items = session.query(Diaper).filter_by(user_id=user_id).all()
        return items


    def get_last_diaper_record(self, user_id):
        last_item = session.query(Diaper).filter_by(user_id=user_id).order_by(desc(Diaper.diaper_id)).first()
        return last_item

